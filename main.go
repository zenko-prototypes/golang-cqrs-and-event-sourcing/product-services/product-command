package main

import (
	"ProductCommandService/event_store"
	"ProductCommandService/product"
)

func main() {
	client, err := event_store.Connect()
	if err != nil {
		panic(err)
	}

	p, err := product.NewProduct("name")
	if err != nil {
		panic(err)
	}

	err = p.UpdateName("Updated name")
	if err != nil {
		panic(err)
	}

	err = p.Save(client)
	if err != nil {
		return
	}

	err = p.UpdateName("New name again")
	if err != nil {
		panic(err)
	}

	err = p.Save(client)
	if err != nil {
		return
	}
}
