package interfaces

import (
	"github.com/google/uuid"
)

type Event interface {
	Handle(aggregate Aggregate) error
	GetName() string
}

type BaseEvent struct {
	AggregateID uuid.UUID `json:"aggregate_id"`
}
