package interfaces

import (
	"github.com/google/uuid"
)

type Aggregate interface {
	On(event Event, version int) error
	Raise(event Event) error
}

type BaseAggregate struct {
	ID                uuid.UUID `json:"id"`
	UncommittedEvents []Event   `json:"uncommitted_events"`
	Version           int       `json:"version"`
}
