package interfaces

type Command interface {
	Handle()
}
