package event_store

import (
	"context"
	"github.com/EventStore/EventStore-Client-Go/esdb"
)

func Connect() (*esdb.Client, error) {
	settings, err := esdb.ParseConnectionString("esdb://localhost:2113?tls=false")

	if err != nil {
		return nil, err
	}

	return esdb.NewClient(settings)
}

func Read(client *esdb.Client, id string) (*esdb.ReadStream, error) {
	return client.ReadStream(context.Background(), "product-"+id, esdb.ReadStreamOptions{}, 10)
}
