# CQRS Projector

## Description

This microservice is here to handle command and write an event into the event store db.
His purpose is to handle write data.
It needs to be started after the projector.

https://gitlab.com/zenko-prototypes/golang-cqrs-and-event-sourcing/product-services/product-projector
https://gitlab.com/zenko-prototypes/golang-cqrs-and-event-sourcing/product-services/product-command
https://gitlab.com/zenko-prototypes/golang-cqrs-and-event-sourcing/product-services/product-query

## Installation

You need to update information about event store db connection in this [file](event_store/event_store.go)

## Run

```bash
$ go run main.go
```