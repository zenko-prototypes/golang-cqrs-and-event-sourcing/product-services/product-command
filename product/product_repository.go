package product

import (
	"ProductCommandService/interfaces"
	"context"
	"encoding/json"
	"github.com/EventStore/EventStore-Client-Go/esdb"
)

func (p *Product) Save(client *esdb.Client) error {
	for _, e := range p.UncommittedEvents {
		data, err := json.Marshal(e)
		if err != nil {
			return err
		}

		_, err = client.AppendToStream(
			context.Background(),
			"product_"+p.ID.String(),
			esdb.AppendToStreamOptions{},
			esdb.EventData{
				ContentType: esdb.JsonContentType,
				EventType:   e.GetName(),
				Data:        data,
			})
		if err != nil {
			return err
		}
	}
	p.UncommittedEvents = []interfaces.Event{}

	return nil
}
