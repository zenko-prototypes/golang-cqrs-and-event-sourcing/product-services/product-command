package product

import (
	"ProductCommandService/interfaces"
	"github.com/google/uuid"
)

type Created struct {
	interfaces.BaseEvent
	ProductID   uuid.UUID `json:"product_id"`
	ProductName string    `json:"product_name"`
}

func (e *Created) Handle(a interfaces.Aggregate) error {
	product := a.(*Product)

	product.ID = e.ProductID
	product.Name = e.ProductName

	return nil
}

func (e *Created) GetName() string {
	return "ProductCreated"
}
