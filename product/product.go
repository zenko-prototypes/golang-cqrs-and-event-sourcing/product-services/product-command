package product

import (
	"ProductCommandService/interfaces"
	"github.com/google/uuid"
)

type Product struct {
	interfaces.BaseAggregate
	Name string `json:"name"`
}

func NewProduct(name string) (*Product, error) {
	id := uuid.New()
	product := &Product{BaseAggregate: interfaces.BaseAggregate{Version: -1}}

	err := product.Raise(&Created{
		BaseEvent: interfaces.BaseEvent{
			AggregateID: id,
		},
		ProductID:   id,
		ProductName: name,
	})

	if err != nil {
		return nil, err
	}

	return product, nil
}

func (p *Product) UpdateName(name string) error {
	return p.Raise(&NameUpdated{
		BaseEvent: interfaces.BaseEvent{
			AggregateID: p.ID,
		},
		ProductName: name,
	})
}

func (p *Product) On(event interfaces.Event, version int) error {
	p.Version = version
	return event.Handle(p)
}

func (p *Product) Raise(event interfaces.Event) error {
	version := p.Version + 1
	err := p.On(event, version)
	if err != nil {
		return err
	}
	p.UncommittedEvents = append(p.UncommittedEvents, event)
	return nil
}
