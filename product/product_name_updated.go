package product

import (
	"ProductCommandService/interfaces"
)

type NameUpdated struct {
	interfaces.BaseEvent
	ProductName string `json:"product_name"`
}

func (e *NameUpdated) Handle(a interfaces.Aggregate) error {
	product := a.(*Product)

	product.Name = e.ProductName

	return nil
}

func (e *NameUpdated) GetName() string {
	return "ProductNameUpdated"
}
